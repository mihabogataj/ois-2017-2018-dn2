// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
  
  var sporociloSplit = sporocilo.besedilo.split(" ");
  for(var i = 0; i < sporociloSplit.length; i++) {
    if (sporociloSplit[i].charAt(0) == '@') {
      var omemba = sporociloSplit[i].substr(1);
      this.socket.emit('sporocilo', {vzdevek: omemba, besedilo: "☞ Omemba v klepetu"});
    }
  }
  
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        var besedilo_out = parametri[parametri.length - 2];
        var uporabniki = parametri[1].split(",");
        
        for (var i = 0; i < uporabniki.length; i++) {
          this.socket.emit('sporocilo', {vzdevek: uporabniki[i].trim(), besedilo: besedilo_out});  
          
          var sporociloSplit = besedilo_out.split(" ");
          for(var j = 0; j < sporociloSplit.length; j++) {
            if (String(sporociloSplit[j]).charAt(0) == '@') {
              var omemba = sporociloSplit[j].substr(1);
              if (omemba == uporabniki[i].trim()) {
                this.socket.emit('sporocilo', {vzdevek: omemba, besedilo: "☞ Omemba v klepetu"});
              }
            }
          }
        }
        
        sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};